use crate::utils;
use chrono::{DateTime, Utc};
use serde::de::{Deserialize, Deserializer, Error};

/// Wrapper for custom format of chrono::Datetime, meaning UTC timestamp in ms
#[derive(Debug, PartialEq)]
pub struct TimestampMs(DateTime<Utc>);

impl From<DateTime<Utc>> for TimestampMs {
    fn from(item: DateTime<Utc>) -> Self {
        TimestampMs(item)
    }
}

impl From<&TimestampMs> for DateTime<Utc> {
    fn from(item: &TimestampMs) -> DateTime<Utc> {
        item.0
    }
}

impl<'de> Deserialize<'de> for TimestampMs {
    fn deserialize<D>(deserializer: D) -> Result<TimestampMs, D::Error>
    where
        D: Deserializer<'de>,
    {
        let value: i64 = Deserialize::deserialize(deserializer)?;
        let date_time = utils::parse_timestamp_ms(value)
            .map_err(|_| Error::custom("Failed to parse timestamp"))?;
        Ok(date_time.into())
    }
}
