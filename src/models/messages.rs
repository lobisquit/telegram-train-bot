use serde::Serialize;
use serde_json;
use std::error::Error;
use ureq;

/// Parse mode, as specified by Telegram API
#[derive(Serialize, Debug, PartialEq)]
pub enum ParseMode {
    #[serde(rename = "HTML")]
    Html,

    #[allow(dead_code)]
    #[serde(rename = "Markdown")]
    Markdown,

    #[allow(dead_code)]
    #[serde(rename = "MarkdownV2")]
    MarkdownV2,
}

/// Message ready to be serialized, as specified by Telegram API
#[derive(Serialize, new, Debug, PartialEq)]
pub struct Message {
    chat_id: i32,
    parse_mode: ParseMode,
    text: String,
}

impl Message {
    /// Send the message using the given BOT token
    pub fn send(&self, token: &str) -> Result<ureq::Response, Box<dyn Error>> {
        let url = format!("https://api.telegram.org/bot{}/sendMessage", token);

        let response = ureq::post(&url)
            .set("X-My-Header", "Secret")
            .send_json(serde_json::to_value(self)?);

        match response {
            Ok(content) => Ok(content),
            Err(ureq::Error::Status(code, _)) => Err(format!("Message sending failed: {}", code))?,
            Err(what) => Err(format!("Unexpected error: {:?}", what))?,
        }
    }
}
