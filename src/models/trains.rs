use crate::models::routes::Route;
use log::{debug, info};
use regex::Regex;
use serde::{Deserialize, Serialize};
use serde_json;
use std::error::Error;
use std::fmt;
use ureq;

/// Train identifier, as reported by ViaggiaTreno API
#[derive(Deserialize, Serialize, Debug, new)]
pub struct Train {
    station_id: String,
    train_id: String,
}

impl fmt::Display for Train {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str(&format!("({}, {})", self.station_id, self.train_id))
    }
}

impl Train {
    fn fetch_complete_train_code(&self) -> Result<String, ViaggiaTrenoError> {
        use ViaggiaTrenoError::*;

        let url = format!("http://www.viaggiatreno.it/viaggiatrenonew/resteasy/viaggiatreno/cercaNumeroTrenoTrenoAutocomplete/{}", self.train_id);
        let response = ureq::get(&url).call()?;
        let body = response.into_string()?;

        // ex: 16306 - VENEZIA S. LUCIA|16306-S02593-1617228000000 -> 1617228000000
        let re = Regex::new(r".*\|\d*-.*-(\d*)").unwrap();
        let matches = re
            .captures_iter(&body)
            .next()
            .ok_or(GenericError("Invalid code completion response".into()))?;

        // note that the whole matching string is found in matches[0]
        return Ok(matches[1].to_string());
    }

    pub fn fetch_route(&self) -> Result<Route, ViaggiaTrenoError> {
        use ViaggiaTrenoError::*;

        let code = self.fetch_complete_train_code()?;
        let train_url = format!(
            "http://www.viaggiatreno.it/viaggiatrenonew/resteasy/viaggiatreno/andamentoTreno/{}/{}/{}",
            self.station_id, self.train_id, code
        );

        info!("Fetching route of {} at {}", self, train_url);

        let response = ureq::get(&train_url).call()?;
        let body = response.into_string()?;

        debug!("{:#?}", body);

        if body.is_empty() {
            Err(GenericError("Response body is empty".into()))
        } else {
            if body.starts_with("Error 404: Could not find resource") {
                Err(ApiDown)
            } else {
                let route = serde_json::from_str::<Route>(&body)?;
                info!("Route of {} parsed correctly", self);

                Ok(route)
            }
        }
    }
}

/// Discriminate between fetching errors
#[derive(Debug)]
pub enum ViaggiaTrenoError {
    ApiDown,
    GenericError(String),
}

impl fmt::Display for ViaggiaTrenoError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use ViaggiaTrenoError::*;

        match self {
            ApiDown => f.write_str("ViaggiaTreno API is probably down"),
            GenericError(msg) => f.write_str(&msg),
        }
    }
}

/// Automatically convert errors to generic ones: create specialized versions directly
impl<E> From<E> for ViaggiaTrenoError
where
    E: Error,
{
    fn from(error: E) -> Self {
        ViaggiaTrenoError::GenericError(error.to_string())
    }
}

#[test]
fn fetch_route_correct() {
    use ViaggiaTrenoError::*;

    // this train should be available all days
    let train = Train::new("S02593".into(), "16310".into());

    let body = train.fetch_route();

    // correct is everything but the generic error
    let is_body_expected = match body {
        Ok(_) => true,
        Err(err) => match err {
            ApiDown => true,
            _ => false,
        },
    };

    assert!(is_body_expected);
}

#[test]
fn fetch_route_failure() {
    let train = Train::new("S02593".into(), "16310ASD".into());

    let body = train.fetch_route();
    assert!(body.is_err());
}
