use crate::models::delays::Delay;
use crate::models::station_names::StationName;
use crate::models::stops::Stop;
use crate::models::timestamp_ms::TimestampMs;
use chrono::{DateTime, FixedOffset, Utc};
use serde::de::{Deserialize, Deserializer};

/// Status of the route, as reported by ViaggiaTreno API
#[derive(Debug, PartialEq)]
pub enum RouteStatus {
    Nominal,
    NotNominal,
}

impl<'de> Deserialize<'de> for RouteStatus {
    fn deserialize<D>(deserializer: D) -> Result<RouteStatus, D::Error>
    where
        D: Deserializer<'de>,
    {
        let value: &str = Deserialize::deserialize(deserializer)?;
        Ok(match value {
            "PG" => RouteStatus::Nominal,
            _ => RouteStatus::NotNominal,
        })
    }
}

/// Route of a train, as reported by ViaggiaTreno API
#[derive(Debug, new, PartialEq, Deserialize)]
pub struct Route {
    #[serde(rename(deserialize = "origineZero"))]
    first_station: StationName,

    #[serde(rename(deserialize = "destinazioneZero"))]
    last_station: StationName,

    #[serde(rename(deserialize = "numeroTreno"))]
    train_id: i64,

    #[serde(rename(deserialize = "tipoTreno"))]
    pub status: RouteStatus,

    #[serde(rename(deserialize = "orarioPartenzaZero"))]
    pub departure_time: Option<TimestampMs>,

    #[serde(rename(deserialize = "fermate"))]
    stops: Vec<Stop>,
}

impl Route {
    pub fn to_html_string(&self, timezone: &FixedOffset) -> String {
        let departure_time_str = match &self.departure_time {
            Some(time_ms) => {
                let departure_localtime = DateTime::<Utc>::from(time_ms).with_timezone(timezone);
                departure_localtime.format("%d/%m").to_string()
            }
            None => "DEPARTURE TIME MISSING".to_string(),
        };

        let mut lines = Vec::new();

        lines.push(format!(
            "{} - treno <code>{}</code>",
            departure_time_str, self.train_id
        ));

        match self.status {
            RouteStatus::Nominal => {
                // empty line
                lines.push("".into());

                // detailed stop information
                for stop in &self.stops {
                    lines.push(stop.to_html_string(timezone))
                }
            }
            RouteStatus::NotNominal => {
                let err_msg = "\n! treno soppresso o parzialmente cancellato !";
                lines.push(err_msg.into())
            }
        }

        // return as a single string
        lines.join("\n")
    }

    pub fn is_late(&self, max_delay: &Delay) -> bool {
        self.stops.iter().any(|s| s.is_late(&max_delay))
    }
}

#[test]
fn parse_route() {
    use crate::utils;
    use chrono::Duration;

    let input = r#"{
        "orarioPartenzaZero": 1615743296000,
        "tipoTreno": "PG",
        "origineZero": "VENEZIA",
        "destinazioneZero": "VENEZIA MESTRE",
        "numeroTreno": 16306,
        "fermate": [
            {
                "stazione": "VENEZIA",
                "programmata": 1615743296000,
                "ritardo": 100
            },
            {
                "stazione": "VENEZIA MESTRE",
                "programmata": 1615743297000,
                "ritardo": 110
            }
        ]
    }"#;

    // this train should be available all days
    let parsed: Route = serde_json::from_str(&input).expect("Failed to parse");

    let mut stops = Vec::new();
    stops.push(Stop::new(
        "VENEZIA".into(),
        utils::parse_timestamp_ms(1615743296000).unwrap().into(),
        Duration::minutes(100).into(),
    ));

    stops.push(Stop::new(
        "VENEZIA MESTRE".into(),
        utils::parse_timestamp_ms(1615743297000).unwrap().into(),
        Duration::minutes(110).into(),
    ));

    let route = Route::new(
        "VENEZIA".into(),
        "VENEZIA MESTRE".into(),
        16306,
        RouteStatus::Nominal,
        Some(utils::parse_timestamp_ms(1615743296000).unwrap().into()),
        stops,
    );

    assert_eq!(route, parsed);
}

#[test]
fn route_to_msg() {
    use crate::utils;
    use chrono::Duration;

    let mut stops = Vec::new();
    stops.push(Stop::new(
        "VENEZIA".into(),
        utils::parse_timestamp_ms(1615743296000).unwrap().into(),
        Duration::minutes(100).into(),
    ));

    stops.push(Stop::new(
        "VENEZIA MESTRE".into(),
        utils::parse_timestamp_ms(1615743297000).unwrap().into(),
        Duration::minutes(110).into(),
    ));

    let route = Route::new(
        "VENEZIA".into(),
        "VENEZIA MESTRE".into(),
        16306,
        RouteStatus::Nominal,
        Some(utils::parse_timestamp_ms(1615743296000).unwrap().into()),
        stops,
    );

    let mut expected_lines = Vec::new();
    expected_lines.push("14/03 - treno <code>16306</code>");
    expected_lines.push("");
    expected_lines.push("<code>17:34 +100min</code>  Venezia");
    expected_lines.push("<code>17:34 +110min</code>  Venezia Mestre");

    let timezone = FixedOffset::east(0);
    assert_eq!(route.to_html_string(&timezone), expected_lines.join("\n"));
}

#[test]
fn route_to_msg_missing_departure_time() {
    use std::fs::File;
    use std::io::Read;

    let mut input_file = File::open("tests/missing-dep-time.json").unwrap();
    let mut data = String::new();
    input_file.read_to_string(&mut data).unwrap();

    serde_json::from_str::<Route>(&data).unwrap();
}
