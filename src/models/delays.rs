use chrono::Duration;
use serde::de::{Deserialize, Deserializer};

/// Wrapper for custom format of chrono::Duration, meaning a time in minutes
#[derive(Debug, PartialEq)]
pub struct Delay(pub Duration);

impl From<Duration> for Delay {
    fn from(item: Duration) -> Self {
        Delay(item)
    }
}

impl From<&Duration> for Delay {
    fn from(item: &Duration) -> Self {
        Delay(*item)
    }
}

impl From<&Delay> for Duration {
    fn from(item: &Delay) -> Duration {
        item.0
    }
}

impl From<Delay> for Duration {
    fn from(item: Delay) -> Duration {
        item.0
    }
}

impl<'de> Deserialize<'de> for Delay {
    fn deserialize<D>(deserializer: D) -> Result<Delay, D::Error>
    where
        D: Deserializer<'de>,
    {
        let value: i64 = Deserialize::deserialize(deserializer)?;
        let delay = Duration::minutes(value);
        Ok(delay.into())
    }
}
