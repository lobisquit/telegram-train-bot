use crate::utils;
use serde::de::{Deserialize, Deserializer};

/// Wrapper for station  name as reported by ViaggiaTreno API, meaning as UPPERCASE strings
#[derive(Debug, PartialEq)]
pub struct StationName(String);

impl<S> From<S> for StationName
where
    S: Into<String>,
{
    fn from(item: S) -> Self {
        let name: String = utils::title_case(item.into());
        let fixed_name = name.replace("e`", "è");

        StationName(fixed_name)
    }
}

impl From<&StationName> for String {
    fn from(item: &StationName) -> String {
        item.0.clone()
    }
}

impl<'de> Deserialize<'de> for StationName {
    fn deserialize<D>(deserializer: D) -> Result<StationName, D::Error>
    where
        D: Deserializer<'de>,
    {
        let value: &str = Deserialize::deserialize(deserializer)?;
        Ok(value.into())
    }
}

#[test]
fn handling_of_backticks() {
    let name: StationName = "ASDE` TEST".into();
    assert_eq!(name, "Asdè Test".into());
}
