use crate::models::delays::Delay;
use crate::models::station_names::StationName;
use crate::models::timestamp_ms::TimestampMs;
use chrono::{DateTime, Duration, FixedOffset, Utc};

/// Train stop, as reported by ViaggiaTreno API
#[derive(Deserialize, Debug, new, PartialEq)]
pub struct Stop {
    #[serde(rename(deserialize = "stazione"))]
    name: StationName,

    #[serde(rename(deserialize = "programmata"))]
    departure_time: TimestampMs,

    #[serde(rename(deserialize = "ritardo"))]
    delay: Delay,
}

impl Stop {
    pub fn to_html_string(&self, timezone: &FixedOffset) -> String {
        let departure_local_time =
            DateTime::<Utc>::from(&self.departure_time).with_timezone(timezone);

        format!(
            "<code>{} {:+}min</code>  {}",
            departure_local_time.format("%H:%M"),
            Duration::from(&self.delay).num_minutes(),
            String::from(&self.name)
        )
    }

    pub fn is_late(&self, max_delay: &Delay) -> bool {
        let current_delay: Duration = self.delay.0;
        let max_delay: Duration = max_delay.0;

        current_delay >= max_delay
    }
}

#[test]
pub fn parse_stop() {
    use crate::utils;
    use chrono::Duration;

    let input = r#"{
        "stazione": "VENEZIA",
        "programmata": 1615743296000,
        "ritardo": 100
    }"#;

    // this train should be available all days
    let parsed = serde_json::from_str::<Stop>(input);
    assert!(parsed.is_ok());

    let stop = Stop::new(
        "Venezia".into(),
        utils::parse_timestamp_ms(1615743296000).unwrap().into(),
        Duration::minutes(100).into(),
    );

    assert_eq!(stop, parsed.unwrap());
}

#[test]
fn parse_stop_array() {
    let input = r#"[{
        "stazione": "VENEZIA",
        "programmata": 1615743296000,
        "ritardo": 100
    },
    {
        "stazione": "VENEZIA MESTRE",
        "programmata": 1615743297000,
        "ritardo": 110
    }]"#;

    // this train should be available all days
    let parsed = serde_json::from_str::<Vec<Stop>>(&input);
    assert!(parsed.is_ok());
}
