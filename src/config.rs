use crate::models::trains::Train;
use chrono::Weekday;
use serde::{Deserialize, Serialize};

/// Structure of the bot configuration
#[derive(Serialize, Deserialize, Debug)]
pub struct RequestConfig {
    /// Time label, possibly the UTC hour
    pub time_label: String,

    /// Always send message related to this request
    pub always_send: bool,

    /// Chat IDs of users interested in this train
    pub chat_ids: Vec<i32>,

    /// Train
    pub train: Train,

    /// Weekdays the train has to be monitored on
    pub weekdays: Vec<Weekday>,

    /// Maximum delay allowed for the user
    pub max_delay_minutes: i64,

    /// Timezone of the user, in seconds, where positive means eastward
    pub timezone_s: i32,
}
