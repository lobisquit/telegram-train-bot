#[macro_use]
extern crate derive_new;

#[macro_use]
extern crate serde;

extern crate log;

extern crate regex;
extern crate ureq;

mod config;
mod utils;
mod models {
    pub mod delays;
    pub mod messages;
    pub mod routes;
    pub mod station_names;
    pub mod stops;
    pub mod timestamp_ms;
    pub mod trains;
}

// external imports
use chrono::{Datelike, Duration, FixedOffset, Utc};
use log::{error, info};
use serde_json;
use std::env;
use std::error::Error;

// internal imports
use crate::config::RequestConfig;
use crate::models::messages::{Message, ParseMode};
use crate::models::routes::RouteStatus;

fn main() -> Result<(), Box<dyn Error>> {
    env_logger::init();

    // load bot configuration
    let token = env::var("BOT_TOKEN")?;

    // load and parse json configuration
    let raw_config = env::var("CONFIG")?;
    let config: Vec<RequestConfig> = serde_json::from_str(&raw_config)?;

    // read time label
    let args: Vec<String> = env::args().collect();
    let error: Box<dyn Error> = "Time label was not provided".to_string().into();
    let time_label = args.get(1).ok_or(error)?;

    // filter the requests with the given label
    let config: Vec<&RequestConfig> = config
        .iter()
        .filter(|item| item.time_label == *time_label)
        .collect();

    for request in config {
        // parse user location and preferences
        let max_delay = Duration::minutes(request.max_delay_minutes);
        let timezone =
            FixedOffset::east_opt(request.timezone_s).ok_or("Invalid CET time offset")?;

        // obtain train information for each stop in the route
        let route_request = request.train.fetch_route();

        let today = Utc::now();
        if request.weekdays.contains(&today.weekday()) {
            for chat_id in &request.chat_ids {
                let message_request = match route_request {
                    Ok(ref route) => {
                        let is_to_send = route.status == RouteStatus::NotNominal
                            || route.is_late(&max_delay.into())
                            || request.always_send;

                        if is_to_send {
                            let route_msg = route.to_html_string(&timezone);

                            for line in route_msg.split('\n') {
                                info!("Message | {}", line);
                            }
                            Some(Message::new(*chat_id, ParseMode::Html, route_msg.clone()))
                        } else {
                            None
                        }
                    }
                    Err(ref error) => {
                        let error_msg = format!(
                            "Error fetching the route timetable for train {}: {}",
                            request.train, error
                        );

                        error!("{}", error_msg);
                        Some(Message::new(*chat_id, ParseMode::Html, error_msg))
                    }
                };

                if let Some(message) = message_request {
                    match message.send(&token) {
                        Ok(_) => info!("Message sent correctly"),
                        Err(err_msg) => error!("{}", err_msg),
                    }
                } else {
                    info!("No alarms to report")
                }
            }
        }
    }
    Ok(())
}
