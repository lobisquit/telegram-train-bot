use chrono::{DateTime, NaiveDateTime, Utc};

/// Convert an import string to title format, meaning each word starts with
/// uppercase and others are lowercase
pub fn title_case(input: String) -> String {
    input
        .split(' ')
        .map(|word| {
            let lower_word = word.to_lowercase();

            // uppercase first char
            let mut c = lower_word.chars();
            match c.next() {
                None => String::new(),
                Some(f) => f.to_uppercase().collect::<String>() + c.as_str(),
            }
        })
        .collect::<Vec<String>>()
        .join(" ")
}

/// Parse a UTC timestamp in ms to a proper DateTime
pub fn parse_timestamp_ms(time_ms: i64) -> Result<DateTime<Utc>, String> {
    let naive_time = NaiveDateTime::from_timestamp_opt(time_ms / 1000, 0)
        .ok_or("Invalid timestamp".to_string())?;
    Ok(DateTime::<Utc>::from_utc(naive_time, Utc))
}
